# encondig: UTF-8

import math
from heapdict import heapdict

_NOT_NODE = object()

class AStar:
    def __init__(self, start, goal, enumerate_outedges, estimate_cost):
        self._start = start
        self._goal = goal
        self._enumerate_outedges = enumerate_outedges
        self._estimate_cost = estimate_cost

        self._backtrace = {start: (_NOT_NODE, 0)}
        self._frontier = heapdict({start: (estimate_cost(start, goal), 0)})
        self._next_tag = -1

    def step(self):
        if not self._frontier:
            return None, math.inf
        node, _ = self._frontier.popitem()
        if node == self._goal:
            path = [node]
            node, cost = self._backtrace[node]
            while node is not _NOT_NODE:
                path.append(node)
                node, _ = self._backtrace[node]
            path.reverse()
            return path, cost
        _, cost_so_far = self._backtrace[node]
        for step_cost, neighbor in self._enumerate_outedges(node):
            _, old_cost = self._backtrace.get(neighbor, (None, math.inf))
            new_cost = cost_so_far + step_cost
            if new_cost >= old_cost:
                continue
            self._backtrace[neighbor] = node, new_cost
            self._frontier[neighbor] = \
                new_cost + self._estimate_cost(neighbor, self._goal), \
                self._next_tag
        return None, cost_so_far

