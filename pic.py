#!/usr/bin/env python
# encondig: UTF-8

import sys
import math
import argparse
import PIL.Image

from astar import AStar

argparser = argparse.ArgumentParser()
argparser.add_argument('image', type=argparse.FileType('rb'))
args = argparser.parse_args()

image = PIL.Image.open(args.image)

start = None
goal = None
for y in range(image.height):
    for x in range(image.width):
        point = x, y
        color = image.getpixel(point)[:3]
        if start is None and color == (255, 0, 0):
            start = point
        if goal is None and color == (0, 0, 255):
            goal = point
if start is None:
    print('No start pixel found', file=sys.stderr)
    sys.exit(1)
if goal is None:
    print('No goal pixel found', file=sys.stderr)
    sys.exit(1)

_neighbor_offsets = tuple((
    (x, y) for y in (-1, 0, +1)
           for x in (-1, 0, +1)
           if (x == 0) != (y == 0)
))
def enumerate_outedges(node):
    x0, y0 = node
    for dx, dy in _neighbor_offsets:
        x = x0 + dx
        y = y0 + dy
        if not (0 <= x < image.width and 0 <= y < image.height):
            continue
        color = image.getpixel((x, y))[:3]
        if color == (0, 0, 0):
            continue
        yield math.sqrt(dx**2 + dy**2), (x, y)

def estimate_cost(start, goal):
    xs, ys = start
    xg, yg = goal
    return math.sqrt((xg - xs)**2 + abs(yg - ys)**2)

astar = AStar(start, goal, enumerate_outedges, estimate_cost)
while True:
    path, distance = astar.step()
    if distance == math.inf:
        print('Goal is unreachable from the start', file=sys.stderr)
        sys.exit(1)
    if path is not None:
        break

print(f'Distance to goal is {distance}')
for x, y in path:
    image.putpixel((x, y), (0, 255, 0, 255))
image.show()

